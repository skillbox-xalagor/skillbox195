#include <iostream>

class Animal
{
public:
	Animal() {}
	virtual void Voice()
	{
		std::cout << "Animal Voice\n";
	}
};

class Dog : public Animal
{
public:
	Dog() {}
	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal
{
public:
	Cat() {}
	void Voice() override
	{
		std::cout << "Meow!\n";
	}
};

class Fish : public Animal
{
public:
	Fish() {}
	void Voice() override
	{
		std::cout << "...\n";
	}
};

int main()
{
	Animal* Animals[3];
	Animals[0] = new Dog();
	Animals[1] = new Cat();
	Animals[2] = new Fish();
	for (auto& i : Animals)
	{
		i->Voice();
	}
}